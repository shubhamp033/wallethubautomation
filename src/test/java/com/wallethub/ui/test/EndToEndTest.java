package com.wallethub.ui.test;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import com.wallethub.ui.pages.HomePage;
import com.wallethub.ui.pages.LoginPage;
import com.wallethub.ui.pages.ProfilePage;
import com.wallethub.ui.pages.ReviewsPage;
import com.wallethub.ui.pages.TestInsuranceCompanyPage;
import com.wallethub.ui.pages.WriteReviewPage;
import com.wallethub.ui.util.TestBase;

public class EndToEndTest extends TestBase {

	HomePage homePage;
	LoginPage loginPage;
	TestInsuranceCompanyPage testInsuranceCompanyPage;
	WriteReviewPage writeReviewPage;
	ProfilePage profilePage;
	ReviewsPage reviewsPage;

	@BeforeMethod
	public void setUp() {
		initialize();

		homePage = new HomePage();
		loginPage = new LoginPage();
		testInsuranceCompanyPage = new TestInsuranceCompanyPage();
		writeReviewPage = new WriteReviewPage();
		profilePage = new ProfilePage();
		reviewsPage = new ReviewsPage();
	}

	@Test
	public void endToEndTest() {
		homePage.navigateToLoginPage();
		loginPage.login();
		homePage.navigateToTestInsuranceCompany();
		testInsuranceCompanyPage.setRating();
		writeReviewPage.selectPolicy();
		writeReviewPage.selectRating();
		writeReviewPage.validateWriteReviewPage();
		writeReviewPage.writeReview();
		writeReviewPage.validateConfirmationMsg();
		homePage.navigateToProfilePage();
		profilePage.validatePostedReview();
		homePage.navigateToReviewPage();
		reviewsPage.validateReview();
	}

	@AfterMethod
	public void closeBrowser() {
		driver.quit();
	}

}
