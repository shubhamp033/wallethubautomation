package com.wallethub.ui.test;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

import com.wallethub.ui.pages.SignUpPage;
import com.wallethub.ui.util.TestBase;

public class SignUpPageTest extends TestBase {

	SignUpPage signUpPage;

	@BeforeMethod
	public void setUp() {
		initialize();
		signUpPage = new SignUpPage(driver);
	}

	@Test
	public void signUpTest() {
		signUpPage.signUp();
	}

	@AfterMethod
	public void closeBrowser() {
		driver.quit();
	}

}
