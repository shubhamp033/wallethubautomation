package com.wallethub.ui.test;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

import com.wallethub.ui.pages.HomePage;
import com.wallethub.ui.pages.LoginPage;
import com.wallethub.ui.util.TestBase;

public class LoginPageTest extends TestBase {

	HomePage homePage;
	LoginPage loginPage;

	@BeforeMethod
	public void setUp() {
		initialize();

		homePage = new HomePage();
		loginPage = new LoginPage();
	}

	@Test
	public void loginPageTest() {
		homePage.navigateToLoginPage();
		loginPage.login();
	}

	@AfterMethod
	public void closeBrowser() {
		driver.quit();
	}

}
