package com.wallethub.ui.util;

import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.asserts.SoftAssert;

public class TestBase {

	public Properties prop;
	public static WebDriver driver;
	public String osName;
	public SoftAssert softAssert = new SoftAssert();
	public static Actions action;

	public TestBase() {
		prop = new Properties();
		try {
			FileInputStream ip = new FileInputStream(
					System.getProperty("user.dir") + "/src/main/java/com/wallethub/ui/config/config.properties");
			prop.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void initialize() {
		osName = System.getProperty("os.name");
		System.out.println("current OS name ::::--->" + osName);

		if (prop.getProperty("browser").equals("chrome")) {
			if (osName.startsWith("Windows")) {
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "/src/main/resources/chromedriver.exe");
			} else if (osName.startsWith("Linux")) {
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "/src/main/resources/chromedriverLinux");

				ChromeDriverService service = new ChromeDriverService.Builder()
						.usingDriverExecutable(
								new File(System.getProperty("user.dir") + "/src/main/resources/chromedriverLinux"))
						.usingAnyFreePort().build();
				try {
					service.start();
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else if (osName.startsWith("Mac")) {
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "/src/main/resources/chromedriver");
			}

			driver = new ChromeDriver();

			if (osName.startsWith("Mac")) {
				java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				org.openqa.selenium.Point position = new Point(0, 0);
				driver.manage().window().setPosition(position);
				org.openqa.selenium.Dimension maximizedScreenSize = new Dimension((int) screenSize.getWidth(),
						(int) screenSize.getHeight());
				driver.manage().window().setSize(maximizedScreenSize);
			}

		} else if (prop.getProperty("browser").equals("FF")) {
			System.setProperty("webdriver.gecko.driver",
					System.getProperty("user.dir") + "/src/main/resources/geckodriver.exe");
			driver = new FirefoxDriver();
		}
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIME_OUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));
		action = new Actions(driver);
	}

	public void waitForSeconds() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

	}

	public static void clickByJS(WebElement element, WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", element);
	}

}
