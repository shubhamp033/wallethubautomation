package com.wallethub.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.wallethub.ui.util.TestBase;

public class SignUpPage extends TestBase{
	
	@FindBy(xpath = "//input[@ng-model='fields.email']")
	private WebElement emailId;
	
	@FindBy(xpath = "//input[@ng-model='fields.password1']")
	private WebElement pwd;
	
	@FindBy(xpath = "//input[@ng-model='fields.password2']")
	private WebElement confirmPwd;
	
	@FindBy(xpath = "//span[text()='Get my free credit score & report']")
	private WebElement checkBox;
	
	@FindBy(xpath = "//span[contains(text(),'Join')]")
	private WebElement joinBtn;
	
	
	public SignUpPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	// SignUp Method
	public void signUp() {
		driver.navigate().to(prop.getProperty("lightUserUrl"));
		emailId.sendKeys(prop.getProperty("emailId"));
		pwd.sendKeys(prop.getProperty("password"));
		confirmPwd.sendKeys(prop.getProperty("password"));
		checkBox.click();
		joinBtn.click();
	}
	

}
