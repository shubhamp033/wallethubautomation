package com.wallethub.ui.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.wallethub.ui.util.TestBase;

public class WriteReviewPage extends TestBase {

	@FindBy(xpath = "//span[text()='Please select your policy']")
	private WebElement policyDD;

	@FindBy(xpath = "//a[text()='Health']")
	private WebElement healthOption;

	@FindBy(xpath = "//a[contains(@onclick,'RatingsClick')]")
	private List<WebElement> ratingOptions;

	@FindBy(xpath = "//span[text()='Write a Review']")
	private WebElement writeReviewHeader;

	@FindBy(xpath = "//textarea[@name='review']")
	private WebElement reviewField;

	@FindBy(xpath = "//input[@value='Submit']")
	private WebElement submitBtn;

	@FindBy(xpath = "//h1[text()='Awesome!']")
	private WebElement confirmationMsg;

	public WriteReviewPage() {
		PageFactory.initElements(driver, this);
	}

	public void selectPolicy() {
		waitForSeconds();
		policyDD.click();
		healthOption.click();
	}

	public void validateWriteReviewPage() {
		softAssert.assertTrue(writeReviewHeader.isDisplayed(), "Write a Review page is not displayed");
	}

	public void selectRating() {
		waitForSeconds();
		ratingOptions.get(Integer.parseInt(prop.getProperty("rating"))-1).click();
	}

	public void writeReview() {
		reviewField.clear();
		reviewField.sendKeys(prop.getProperty("reviewContains"));
		submitBtn.click();
	}

	public void validateConfirmationMsg() {
		softAssert.assertTrue(confirmationMsg.isDisplayed(), "Confirmation Message is not displayed");
	}

}
