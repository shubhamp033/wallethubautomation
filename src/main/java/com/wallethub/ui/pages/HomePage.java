package com.wallethub.ui.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.wallethub.ui.util.TestBase;

public class HomePage extends TestBase {

	@FindBy(xpath = "//a[@class='login']")
	private WebElement loginLink;

	@FindBy(xpath = "//a[@class='user']")
	private WebElement userLink;

	@FindBy(xpath = "//a[contains(text(),'Profile')]")
	private WebElement profileLink;

	@FindBy(xpath = "//h2[@class='username']")
	private WebElement username;

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	public void navigateToLoginPage() {
		loginLink.click();
	}

	public void navigateToTestInsuranceCompany() {
		driver.navigate().to(prop.getProperty("testInsuranceCompany"));
	}

	public void navigateToProfilePage() {
		userLink.click();
		profileLink.click();
	}

	public void navigateToReviewPage() {
		String reviewPageUrl = "https://wallethub.com/profile/" + username.getText().replaceAll("@", "").trim()
				+ "/reviews";
		driver.navigate().to(reviewPageUrl);
	}
}
