package com.wallethub.ui.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.wallethub.ui.util.TestBase;

public class LoginPage extends TestBase {

	@FindBy(xpath = "//input[@ng-model='fields.email']")
	private WebElement emailIdField;

	@FindBy(xpath = "//input[@ng-model='fields.password']")
	private WebElement pwdField;

	@FindBy(xpath = "//label[@class='toggle inline-block small']")
	private WebElement remenberMyEmailToggleBtn;

	@FindBy(xpath = "//span[text()='Login']")
	private WebElement loginBtn;

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void login() {
		emailIdField.sendKeys(prop.getProperty("emailId"));
		pwdField.sendKeys(prop.getProperty("password"));
		remenberMyEmailToggleBtn.click();
		loginBtn.click();
		waitForSeconds();
	}
}
