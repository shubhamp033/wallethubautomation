package com.wallethub.ui.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.wallethub.ui.util.TestBase;

public class ProfilePage extends TestBase {

	@FindBy(xpath = "//p[@class='feeddesc']")
	private List<WebElement> reviewList;

	public ProfilePage() {
		PageFactory.initElements(driver, this);
	}

	public void validatePostedReview() {
		String actualReview = reviewList.get(0).getText().toString().trim();
		String expectedReview = prop.getProperty("reviewContains");
		softAssert.assertEquals(actualReview, expectedReview);
	}
}
