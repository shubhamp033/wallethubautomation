package com.wallethub.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.wallethub.ui.util.TestBase;

public class TestInsuranceCompanyPage extends TestBase {

	@FindBy(xpath = "//span[contains(text(),'Rating')]")
	private WebElement ratingStar;

	@FindBy(xpath = "//a[@class='hover']")
	private List<WebElement> ratingOptions;

	public TestInsuranceCompanyPage() {
		PageFactory.initElements(driver, this);
	}

	public void setRating() {
		waitForSeconds();
		action.moveToElement(ratingStar).build().perform();
		WebElement rating = driver.findElement(By.xpath("//a[text()='" + prop.getProperty("rating") + "']"));
		validateRatingStarLitUp(rating);
		rating.click();
	}

	public void validateRatingStarLitUp(WebElement rating) {
		softAssert.assertTrue(rating.isDisplayed(), "Stars are not getting Lit Up");
	}
}
