package com.wallethub.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.wallethub.ui.util.TestBase;

public class ReviewsPage extends TestBase{
	 
	public void validateReview() {
		WebElement expectedReview = driver.findElement(By.xpath("//p[contains(text(),'"+prop.getProperty("reviewContains")+"')]"));
		softAssert.assertTrue(expectedReview.isDisplayed(), "Expected Review is not displayed");
	}

}
